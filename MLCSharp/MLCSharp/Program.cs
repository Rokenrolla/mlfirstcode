﻿using MLCSharpML.Model;
using System;

namespace MLCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Add input data
            var input = new ModelInput();
            int i = 0;
            while (i == 0)
            {
                Console.WriteLine("Input your featured sentiment:");
                input.Sentiment = Console.ReadLine().ToString();
                // Load model and predict output of sample data
                ModelOutput result = ConsumeModel.Predict(input);
                Console.WriteLine($"Text: {input.Sentiment}\nIs the predicted sentiment a positive one? : {result.Prediction}");
            }
        }
    }
}
